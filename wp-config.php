<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'dOvZWElC2.V0.pms' );

/** MySQL hostname */
define( 'DB_HOST', '172.26.10.174:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'b<F`)nlVD,8K-9Q1:tr|!Bwp0Be 2qf6h`Uy9(VY0YdZ?4W60{Wr6n)TKT?)vY/U' );
define( 'SECURE_AUTH_KEY',  'D%r-p@?#zI(kjX`*7:8P62wfZC1syOr&AwnX5I?Gm=)8Fkf<e0/cX:Z.qD4PqTv>' );
define( 'LOGGED_IN_KEY',    'bGWf+T(:AF6p9GyY;Jd@Ybx;P`V$ViMT{9:u-$+R%]$B[/_sPX&0fe4S!0-XB[(D' );
define( 'NONCE_KEY',        '{dUJ}k`MhDI-Wd{y7NE1kK}R&+r7wLPN$z3+A*6lCh2cd(6#(tR1#]!rwi||x0m=' );
define( 'AUTH_SALT',        'Qh9hg.g`PYDmRZh*@_Y3>3N:SNGq>? :k:L[U,;]sXJpe$Ia6utKq.}1 )24G!)o' );
define( 'SECURE_AUTH_SALT', 'QD%fQq{MKxDxV;fn+&^%S^c3#|x)7I%WQCWY%jOCcedf)I NElxabJQ78TPUW0;w' );
define( 'LOGGED_IN_SALT',   '{GJD)roUX1JR:Y%I&v=Cohy%N^S~b^*Ft;B[?_*iZ#cOVt@tk1$sz=Jib.F8QIQM' );
define( 'NONCE_SALT',       'l@<Jfq]tZrh?1:0r$7rNRW|Gs+L;,L7@`U6$[atT1->LfS&#IZd[vD<TB3pn],hb' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
