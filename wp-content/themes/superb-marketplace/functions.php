<?php 
add_action( 'wp_enqueue_scripts', 'superb_marketplace_enqueue_styles_and_scripts' );
function superb_marketplace_enqueue_styles_and_scripts() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
		wp_enqueue_style( 'superb-marketplace-google-fonts', '//fonts.googleapis.com/css?family=Noto+Sans+JP:wght@400;500;700' ); 

} 


function superb_marketplace_customize_register( $wp_customize ) {

	function superb_marketplace_sanitize_checkbox( $input ) {
		return ( ( isset( $input ) && true == $input ) ? true : false );
	}
	$wp_customize->add_section( 'superb_marketplace_unique', array(
		'title'      => __('Superb Marketplace','superb-marketplace'),
		'description'      => __('These are child theme only features','superb-marketplace'),
		'priority'   => 1,
		'capability' => 'edit_theme_options',
		) );


	$wp_customize->add_setting( 'sm_show_post_date', array(
		'default' => 0,
		'sanitize_callback' => 'superb_marketplace_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'sm_show_post_date', array(
		'label'    => __( 'Show post meta', 'superb-marketplace' ),
		'section'  => 'superb_marketplace_unique',
		'priority' => 1,
		'settings' => 'sm_show_post_date',
		'type'     => 'checkbox',
	) );

	$wp_customize->add_setting( 'sm_show_comments', array(
		'default' => 0,
		'sanitize_callback' => 'superb_marketplace_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'sm_show_comments', array(
		'label'    => __( 'Show Comments On Woocommerce Product Pages', 'superb-marketplace' ),
		'section'  => 'superb_marketplace_unique',
		'priority' => 1,
		'settings' => 'sm_show_comments',
		'type'     => 'checkbox',
	) );




}
add_action( 'customize_register', 'superb_marketplace_customize_register' );
if(! function_exists('superb_marketplace_customize_register_output' ) ):
	function superb_marketplace_customize_register_output(){
		?>
		<style type="text/css">
			.entry-content-read-more-wrapper a{ color: <?php echo esc_attr(get_theme_mod( 'readmore_button_text')); ?>; }
			.entry-content-read-more-wrapper a{ background: <?php echo esc_attr(get_theme_mod( 'readmore_button_background')); ?>; }
			.main-navigation a, #site-navigation span.dashicons.dashicons-menu:before, .iot-menu-left-ul a { color: <?php echo esc_attr(get_theme_mod( 'navigation_link_color')); ?>; }
			.cart-customlocation svg{ fill: <?php echo esc_attr(get_theme_mod( 'navigation_link_color')); ?>; }
			.navigation-wrapper, .main-navigation ul ul, #iot-menu-left, .cart-preview{ background: <?php echo esc_attr(get_theme_mod( 'navigation_background_color')); ?>; }
			<?php if ( get_theme_mod( 'hide_navigation' ) == '1' ) : ?>
			.navigation-wrapper {display: none;}
		<?php endif; ?>
		<?php if ( get_theme_mod( 'display_navigation_tagline' ) == '1' ) : ?>
		.site-description {display:block;}
		.main-navigation a {line-height:63px;}
		.cart-customlocation svg{margin-top:34px;}
		#site-navigation span.dashicons.dashicons-menu {margin-top:25px;}
	<?php endif; ?>

	<?php if ( get_theme_mod( 'navigation_remove_cart' ) == '1' ) : ?>
	.cart-header {display: none;}
<?php endif; ?>
<?php if ( get_theme_mod( 'hide_addtocart' ) == '1' ) : ?>
	ul.products li.product a.button {display: none;}
<?php endif; ?>
<?php if ( get_theme_mod( 'sm_show_comments' ) == '1' ) : ?>
.single-product div#comments {
	display:block;
}
<?php endif; ?>
<?php if ( get_theme_mod( 'hide_featured_image' ) == '1' ) : ?>
	.single-post .post-thumbnail {
		display: none;
	}
<?php endif; ?>

/* Customize */
ul.products li.product { background: <?php echo esc_attr(get_theme_mod( 'products_bg_select')); ?>; }
.single .content-area a, .page .content-area a, .woocommerce table.shop_table a { color: <?php echo esc_attr(get_theme_mod( 'global_link')); ?>; }
.page .content-area a.button, .single .page .content-area a.button {color:#fff;}
a.button,a.button:hover,a.button:active,a.button:focus, button, input[type="button"], input[type="reset"], input[type="submit"] { background: <?php echo esc_attr(get_theme_mod( 'global_link')); ?>; }
.tags-links a, .cat-links a{ border-color: <?php echo esc_attr(get_theme_mod( 'global_link')); ?>; }
.single main article .entry-meta *, .single main article .entry-meta, .archive main article .entry-meta *, .comments-area .comment-metadata time{ color: <?php echo esc_attr(get_theme_mod( 'global_byline')); ?>; }
.single .content-area h1, .single .content-area h2, .single .content-area h3, .single .content-area h4, .single .content-area h5, .single .content-area h6, .page .content-area h1, .page .content-area h2, .page .content-area h3, .page .content-area h4, .page .content-area h5, .page .content-area h6, .page .content-area th, .single .content-area th, .blog.related-posts main article h4 a, .single b.fn, .page b.fn, .error404 h1, .search-results h1.page-title, .search-no-results h1.page-title, .archive h1.page-title, .page header.entry-header h1, h2.woocommerce-loop-product__title, .woocommerce-billing-fields label,#order_comments_field label, .wc_payment_method label, form.woocommerce-EditAccountForm.edit-account legend, .product h1.product_title.entry-title, .woocommerce div.product p.price *{ color: <?php echo esc_attr(get_theme_mod( 'global_headline')); ?>; }
.comment-respond p.comment-notes, .comment-respond label, .page .site-content .entry-content cite, .comment-content *, .about-the-author, .page code, .page kbd, .page tt, .page var, .page .site-content .entry-content, .page .site-content .entry-content p, .page .site-content .entry-content li, .page .site-content .entry-content div, .comment-respond p.comment-notes, .comment-respond label, .single .site-content .entry-content cite, .comment-content *, .about-the-author, .single code, .single kbd, .single tt, .single var, .single .site-content .entry-content, .single .site-content .entry-content p, .single .site-content .entry-content li, .single .site-content .entry-content div, .error404 p, .search-no-results p, .woocommerce-Price-amount.amount, .woocommerce ul.products li.product .price, mark.count, p.woocommerce-result-count, .cart-subtotal span.woocommerce-Price-amount.amount, .order-total span.woocommerce-Price-amount.amount, .woocommerce-terms-and-conditions-wrapper .validate-required label, .woocommerce-form-login span, .woocommerce-form-login label, .create-account span, #customer_login .form-row label, .woocommerce-view-order mark,.woocommerce-view-order ins, table tfoot, .woocommerce form .form-row label, .payment_method_stripe label, .variations label, .product span.sku, .woocommerce div.product .woocommerce-tabs ul.tabs li a, .woocommerce div.product .woocommerce-tabs ul.tabs li a:hover, .woocommerce table.shop_attributes th, .woocommerce table.shop_attributes td { color: <?php echo esc_attr(get_theme_mod( 'global_content')); ?>; }
.page .entry-content blockquote, .single .entry-content blockquote, .comment-content blockquote { border-color: <?php echo esc_attr(get_theme_mod( 'global_content')); ?>; }
.error-404 input.search-field, .about-the-author, .comments-title, .related-posts h3, .comment-reply-title,#add_payment_method .cart-collaterals .cart_totals tr td, #add_payment_method .cart-collaterals .cart_totals tr th, .woocommerce-cart .cart-collaterals .cart_totals tr td, .woocommerce-cart .cart-collaterals .cart_totals tr th, .woocommerce-checkout .cart-collaterals .cart_totals tr td, .woocommerce-checkout .cart-collaterals .cart_totals tr th, .woocommerce-cart .cart_totals h2, .woocommerce table.shop_table td, .woocommerce-checkout .woocommerce-billing-fields h3, #add_payment_method #payment ul.payment_methods, .woocommerce-cart #payment ul.payment_methods, .woocommerce-checkout #payment ul.payment_methods,.woocommerce div.product .woocommerce-tabs ul.tabs::before { border-color: <?php echo esc_attr(get_theme_mod( 'global_borders')); ?>; }
.product h1.product_title.entry-title:after, .woocommerce-cart h1:after, .woocommerce-account.woocommerce-page h1.entry-title:after, #customer_login h2:after{ background: <?php echo esc_attr(get_theme_mod( 'global_borders')); ?>; }
.woocommerce table.shop_table.woocommerce-checkout-review-order-table, .single article.post table *,.page article.page table *, nav.woocommerce-MyAccount-navigation li{ border-color: <?php echo esc_attr(get_theme_mod( 'global_borders')); ?> !important; }
.wp-block-button__link, ul li.product .button, ul li.product .button:hover, .woocommerce ul.products li.product .product-feed-button .add_to_cart_button, .woocommerce ul.products li.product .product-feed-button .button, .woocommerce ul.products li.product:hover a.added_to_cart.wc-forward, .woocommerce nav.woocommerce-pagination ul li a, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce nav.woocommerce-pagination ul li span.current:hover, .woocommerce nav.woocommerce-pagination ul li span, .woocommerce nav.woocommerce-pagination ul li, a.checkout-button.button.alt.wc-forward, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce table.shop_table .coupon button.button, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .return-to-shop a.button.wc-backward, .woocommerce #respond input#submit.disabled:hover, .woocommerce #respond input#submit:disabled:hover, .woocommerce #respond input#submit:disabled[disabled]:hover, .woocommerce a.button.disabled:hover, .woocommerce a.button:disabled:hover, .woocommerce a.button:disabled[disabled]:hover, .woocommerce button.button.disabled:hover, .woocommerce button.button:disabled:hover, .woocommerce button.button:disabled[disabled]:hover, .woocommerce input.button.disabled:hover, .woocommerce input.button:disabled:hover, .woocommerce input.button:disabled[disabled]:hover, .woocommerce-checkout button#place_order, .woocommerce .woocommerce-message a.button.wc-forward, .woocommerce-message a.button.wc-forward:hover, .woocommerce-message a.button.wc-forward:focus, div#customer_login form.woocommerce-EditAccountForm.edit-account button.woocommerce-Button.button, .woocommerce-form-login button.woocommerce-Button.button, #customer_login button.woocommerce-Button.button, a.button, a.button:hover, a.button:active, a.button:focus, button, input[type="button"], input[type="reset"], input[type="submit"], .woocommerce-account a.woocommerce-button.button.view, .woocommerce-account a.woocommerce-button.button.view:hover, .woocommerce-account a.woocommerce-button.button.view:active, .woocommerce-account a.woocommerce-button.button.view:focus, .woocommerce .woocommerce-MyAccount-content a.button, .woocommerce .woocommerce-MyAccount-content a.button:hover, .woocommerce .woocommerce-MyAccount-content a.button:active, .woocommerce .woocommerce-MyAccount-content a.button:focus, form#add_payment_method button#place_order, .woocommerce-Address a.edit, .woocommerce table a.button.delete, .woocommerce table a.button.delete:hover, button.single_add_to_cart_button.button.alt, button.single_add_to_cart_button.button.alt:hover, .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt:disabled[disabled], .woocommerce #respond input#submit.alt:disabled[disabled]:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt:disabled[disabled], .woocommerce a.button.alt:disabled[disabled]:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt:disabled[disabled], .woocommerce button.button.alt:disabled[disabled]:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt:disabled[disabled], .woocommerce input.button.alt:disabled[disabled]:hover, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt{ background: <?php echo esc_attr(get_theme_mod( 'global_button_background')); ?>; }
.single .content-area a.wp-block-button__link, .page .content-area a.wp-block-button__link, .wp-block-button__link, ul li.product .button, ul li.product .button:hover, .woocommerce ul.products li.product .product-feed-button .add_to_cart_button, .woocommerce ul.products li.product .product-feed-button .button, .woocommerce ul.products li.product:hover a.added_to_cart.wc-forward, .woocommerce nav.woocommerce-pagination ul li a, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce nav.woocommerce-pagination ul li span.current:hover, .woocommerce nav.woocommerce-pagination ul li span, .woocommerce nav.woocommerce-pagination ul li, a.checkout-button.button.alt.wc-forward, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce table.shop_table .coupon button.button, .woocommerce table.shop_table input#coupon_code, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, #secondary .search-form input.search-submit, .search-form input.search-submit, input.search-submit, a.button, a.button:hover, a.button:active, a.button:focus, button, input[type="button"], input[type="reset"], input[type="submit"], .woocommerce-Address a.edit, .woocommerce table a.button.delete, .woocommerce table a.button.delete:hover,button.single_add_to_cart_button.button.alt, button.single_add_to_cart_button.button.alt:hover, .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt:disabled[disabled], .woocommerce #respond input#submit.alt:disabled[disabled]:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt:disabled[disabled], .woocommerce a.button.alt:disabled[disabled]:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt:disabled[disabled], .woocommerce button.button.alt:disabled[disabled]:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt:disabled[disabled], .woocommerce input.button.alt:disabled[disabled]:hover, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt { color: <?php echo esc_attr(get_theme_mod( 'global_button_text')); ?> !important; }
.woocommerce table.shop_table input#coupon_code, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce-account a.woocommerce-button.button.view, .woocommerce-account a.woocommerce-button.button.view:hover, .woocommerce-account a.woocommerce-button.button.view:active, .woocommerce-account a.woocommerce-button.button.view:focus, .woocommerce .woocommerce-MyAccount-content a.button, .woocommerce .woocommerce-MyAccount-content a.button:hover, .woocommerce .woocommerce-MyAccount-content a.button:active, .woocommerce .woocommerce-MyAccount-content a.button:focus, form#add_payment_method button#place_order, .woocommerce-Address a.edit,.woocommerce table a.button.delete, .woocommerce table a.button.delete:hover, button.single_add_to_cart_button.button.alt, button.single_add_to_cart_button.button.alt:hover,.woocommerce .product .woocommerce-tabs ul.tabs.wc-tabs li.active,.woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt:disabled[disabled], .woocommerce #respond input#submit.alt:disabled[disabled]:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt:disabled[disabled], .woocommerce a.button.alt:disabled[disabled]:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt:disabled[disabled], .woocommerce button.button.alt:disabled[disabled]:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt:disabled[disabled], .woocommerce input.button.alt:disabled[disabled]:hover, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt{ border-color: <?php echo esc_attr(get_theme_mod( 'global_button_background')); ?> !important; }
.woocommerce span.onsale { color: <?php echo esc_attr(get_theme_mod( 'global_sale_text')); ?>; }
.woocommerce span.onsale { background: <?php echo esc_attr(get_theme_mod( 'global_sale_bg')); ?>; }
.woocommerce .woocommerce-ordering select, .woocommerce .quantity input.qty, .woocommerce form input, .woocommerce form .form-row .input-text, .woocommerce-page form .form-row .input-text, .select2-container--default .select2-selection--single, .error-404 input.search-field, div#stripe-card-element, div#stripe-exp-element, div#stripe-cvc-element, .woocommerce div.product form.cart .variations select { background: <?php echo esc_attr(get_theme_mod( 'global_input_bg')); ?>; }
.woocommerce .woocommerce-ordering select, .woocommerce .quantity input.qty, .woocommerce form input, .woocommerce form .form-row .input-text, .woocommerce-page form .form-row .input-text, .select2-container--default .select2-selection--single, .error-404 input.search-field, .select2-container--default .select2-selection--single .select2-selection__rendered, div#stripe-card-element, div#stripe-exp-element, div#stripe-cvc-element, .woocommerce div.product form.cart .variations select { color: <?php echo esc_attr(get_theme_mod( 'global_input_text')); ?>; }
.woocommerce .woocommerce-ordering select, .woocommerce .quantity input.qty, .woocommerce form input, .woocommerce form .form-row .input-text, .woocommerce-page form .form-row .input-text, .select2-container--default .select2-selection--single, .woocommerce form .form-row.woocommerce-validated .select2-container, .woocommerce form .form-row.woocommerce-validated input.input-text, .woocommerce form .form-row.woocommerce-validated select, .error-404 input.search-field, div#stripe-card-element, div#stripe-exp-element, div#stripe-cvc-element, .woocommerce div.product form.cart .variations select { border-color: <?php echo esc_attr(get_theme_mod( 'global_input_bg')); ?> !important; }
.select2-container--default .select2-selection--single .select2-selection__arrow b{ border-color: <?php echo esc_attr(get_theme_mod( 'global_input_text')); ?> transparent transparent transparent; }
.single article.post table *,.page article.page table *, .woocommerce .woocommerce-checkout #payment ul.payment_methods, .woocommerce-error, .woocommerce-info, .woocommerce-message, .woocommerce-checkout form.woocommerce-form.woocommerce-form-login.login{ background: #<?php echo esc_attr(get_theme_mod( 'background_color')); ?>; }
body.custom-background.blog, body.blog, body.custom-background.archive, body.archive, body.custom-background.search-results, body.search-results{ background-color: <?php echo esc_attr(get_theme_mod( 'blog_site_background')); ?>; }
.blog main article, .search-results main article, .archive main article, .related-posts.blog main article{ background-color: <?php echo esc_attr(get_theme_mod( 'blog_post_background')); ?>; }
.blog main article h2 a, .search-results main article h2 a, .archive main article h2 a{ color: <?php echo esc_attr(get_theme_mod( 'blog_post_headline')); ?>; }
.blog main article .entry-meta, .archive main article .entry-meta, .search-results main article .entry-meta{ color: <?php echo esc_attr(get_theme_mod( 'blog_post_byline')); ?>; }
.blog main article p, .search-results main article p, .archive main article p { color: <?php echo esc_attr(get_theme_mod( 'blog_post_excerpt')); ?>; }
.nav-links span, .nav-links a, .pagination .current, .nav-links span:hover, .nav-links a:hover, .pagination .current:hover { background: <?php echo esc_attr(get_theme_mod( 'blog_post_navigation_bg')); ?>; }
.nav-links span, .nav-links a, .pagination .current, .nav-links span:hover, .nav-links a:hover, .pagination .current:hover{ color: <?php echo esc_attr(get_theme_mod( 'blog_post_navigation_link')); ?>; }

<?php if ( get_theme_mod( 'fullwidth_productpages' ) == '1' ) : ?>
	.single-product div#primary.content-area { width: 100%; max-width: 100%; }
	.single-product aside#secondary { display: none; }
<?php endif; ?>
<?php if ( get_theme_mod( 'hide_addtocart' ) == '1' ) : ?>
	ul.products li.product a.button {display: none;}
<?php endif; ?>

<?php if ( get_theme_mod( 'postpage_related_products' ) == '1' ) : ?>
	section.related.products {display: none;}
<?php endif; ?>

</style>
<?php }
add_action( 'wp_head', 'superb_marketplace_customize_register_output' );
endif;



/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for child theme Superb Marketplace for publication on WordPress.org
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/tgm/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/tgm/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/tgm/class-tgm-plugin-activation.php';
 */
require_once get_stylesheet_directory() . '/tgm/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'superb_marketplace_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function superb_marketplace_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(
		array(
			'name'        => 'Dokan Lite',
			'slug'        => 'dokan-lite',
		),

	);

	$config = array(
		'id'           => 'superb-marketplace',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
